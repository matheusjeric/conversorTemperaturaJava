package Conversor;

public class MedidorTemperatura {
	
	private float temperatura;
	
	public String converter(float temperatura){
		float valorCelsius = temperatura - 273;
		float valorFahrenheit = ((temperatura-273)/5)*9 + 32;
		
		return "Celsius = " +valorCelsius + "Fahrenheit = " +valorFahrenheit;
	}

	public float getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(float temperatura) {
		this.temperatura = temperatura;
	}

}
