package Conversor;

import java.util.Scanner;


public class Main {
	
	public static void main(String[] args) {
		
		int opcao;
		float valor;
		MedidorTemperatura umaTemperatura = null;
		
		Scanner leitorEntrada = new Scanner(System.in);
		System.out.println("Digite o tipo da temperatura de entrada: ");
		System.out.println("1 - Klevin");
		System.out.println("2 - Celsius");
		System.out.println("3 - Fahrenheit");
		opcao = leitorEntrada.nextInt();
		
		System.out.println("Digite a temperatura de entrada: ");
		valor= leitorEntrada.nextFloat();
		
		if (opcao==1){
			umaTemperatura = new Kelvin();
			
		}
		else if  (opcao==2){
			umaTemperatura = new Celsius();
			
		}
		else if (opcao==3){
			umaTemperatura = new Fahrenheit();
		}
		
		System.out.println(umaTemperatura.converter(valor));
		
		
	}

}
