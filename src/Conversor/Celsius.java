package Conversor;

public class Celsius extends MedidorTemperatura {
	
	public String converter(float temperatura){
		float valorKelvin = temperatura + 273;
		float valorFahrenheit = ((temperatura)/5)*9 + 32;
		
		return "Kelvin = " +valorKelvin + "Fahrenheit = " +valorFahrenheit;
	}
	

}
