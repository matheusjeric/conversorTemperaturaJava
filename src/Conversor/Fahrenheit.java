package Conversor;

public class Fahrenheit extends MedidorTemperatura{
	
	public String converter(float temperatura){
		float valorKelvin = ((temperatura-32)/9)*5 + 273;
		float valorCelsius = ((temperatura-32)/9)*5;
		
		return "Celsius = " +valorKelvin + "Fahrenheit = " +valorCelsius;
	}

}
